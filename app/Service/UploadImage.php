<?php

namespace App\Service;

trait UploadImage
{
    public function storeImage($file, $path = '')
    {
        $name = time().'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path('/image/' . $path);
        $file->move($destinationPath, $name);
        return 'image/' . $path . '/' . $name;
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function deleteImage($path)
    {
        return @unlink(public_path('/' . $path));
    }

    public function deleteImagePackage($path, $object) {
        $root_path = public_path('/image/' . $path . '/' . $object->id);
        if (is_dir($root_path)) {
            \File::cleanDirectory($root_path);
        }
    }

    public function storeImagePackage($file, $path, $object) {
        $root_path = public_path('/image/' . $path);
        $origin_path = $root_path . '/' . $object->id . '/origin';
        $thumbnail_path = $root_path . '/' . $object->id . '/thumbnail';
        $medium_path = $root_path . '/' . $object->id . '/medium';


        if (!is_dir($root_path . '/' . $object->id)) {
            \File::makeDirectory($root_path . '/' . $object->id, 0755, true, true);// folder id resource
        }

        if (!is_dir($origin_path)) {
            \File::makeDirectory($origin_path, 0755, true, true);// origin
        }
        
        if (!is_dir($thumbnail_path)) {
            \File::makeDirectory($thumbnail_path, 0755, true, true);// thumbnail
        }

        if (!is_dir($medium_path)) {
            \File::makeDirectory($medium_path, 0755, true, true);// medium
        }

        $name = $file->getClientOriginalName();

        $image = \Image::make($file);

        $image->save($origin_path . '/' . $name);
        $image->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($thumbnail_path . '/' . $name);

        $image->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($medium_path . '/' . $name);

        return $name;
    }
}
