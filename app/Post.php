<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $fillable = [
        'title', 'slug', 'description', 'post_category_id', 'seo_title', 'seo_description', 'avatar'
    ];

    public function rules($id = null) {
        return [
            'post.title' => 'required|max:200|unique:posts,title,'. $id,
            'post.avatar' => 'image|max:1024',
            'post.description' => 'required',
            'post.slug' => 'max:150',
            'post.post_category_id' => 'required',
            'post.seo_title' => 'max:60',
            'post.seo_description' => 'max:158'
        ];
    }

    public function post_category() {
    	return $this->belongsTo('App\PostCategory');
    }

    public function img($mode = 'thumbnail') {
    	return 'image/posts/'. $this->id . '/' . $mode . '/' . $this->avatar;
    }
}
