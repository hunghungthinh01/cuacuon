<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    public function posts() {
    	return $this->hasMany('App\Post');
    }

    public function posts_with_paginate() {
    	return $this->posts()->orderBy('updated_at', 'desc')->paginate(20);
    }
}
