<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigCommon extends Model
{
    public function rules() {
    	return [
            'setting.comp_name' => 'required|max:250',
            'setting.comp_addr' => 'required',
            'setting.comp_tel' => 'required',
            'setting.comp_fax' => 'max:25',
            'setting.comp_hotline' => 'max:25',
            'setting.comp_email' => 'max:50',
            'setting.comp_website' => 'max:50',
            'setting.header_top' => 'image|max:1024' .  (is_null($this->id) ? '' : '')
        ];
    }
}
