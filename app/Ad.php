<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
	protected $fillable = [
        'title', 'order', 'avatar', 'redirect_to'
    ];

    public function rules($id = null) {
        return [
            'ad.title' => 'required|max:100|unique:ads,title,'. $id,
            'ad.avatar' => 'image|max:1024' .  (is_null($id) ? '|required' : ''),
            'ad.redirect_to' => 'required',
            'ad.order' => 'required'
        ];
    }
}
