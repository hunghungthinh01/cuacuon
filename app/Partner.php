<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
	protected $fillable = [
        'partner_name', 'order', 'avatar', 'redirect_to'
    ];

    public function rules($id = null) {
        return [
            'partner.partner_name' => 'required|max:100|unique:partners,partner_name,'. $id,
            'partner.avatar' => 'image|max:1024' .  (is_null($id) ? '|required' : ''),
            'partner.redirect_to' => 'required',
            'partner.order' => 'required'
        ];
    }
}
