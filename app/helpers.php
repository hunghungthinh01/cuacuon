<?php

if (! function_exists('treeNav')) {
    function treeNav($clear = false) {
    	if ($clear) {
    		Cache::forget('treeNav');
    	}
		$treeNav = Cache::remember('treeNav', 360000, function () {
		    return App\Category::treeNav();
		});
        return $treeNav;
    }
}

if (! function_exists('allPartners')) {
    function allPartners($clear = false) {
    	if ($clear) {
    		Cache::forget('allPartners');
    	}
		$allPartners = Cache::remember('allPartners', 360000, function () {
		    return App\Partner::all();
		});
        return $allPartners;
    }
}

if (! function_exists('allAds')) {
    function allAds($clear = false) {
        if ($clear) {
            Cache::forget('allAds');
        }
        $allAds = Cache::remember('allAds', 360000, function () {
            return App\Ad::orderBy('order')->get();
        });
        return $allAds;
    }
}


if (! function_exists('allServices')) {
    function allServices($clear = false) {
    	if ($clear) {
    		Cache::forget('allServices');
    	}
		$allServices = Cache::remember('allServices', 360000, function () {
		    return App\ServiceCustomer::all();
		});
        return $allServices;
    }
}

if (! function_exists('allSettings')) {
    function allSettings($clear = false) {
        if ($clear) {
            Cache::forget('allSettings');
        }
        $allSettings = Cache::remember('allSettings', 360000, function () {
            return unserialize(App\ConfigCommon::where('code', 'COM')->first()->value);
        });
        return $allSettings;
    }
}

if (! function_exists('allSlides')) {
    function allSlides($position = 'top', $clear = false) {
        if ($clear) {
            Cache::forget('allSlides_' . $position);
        }

        $allSlides = Cache::remember('allSlides_' . $position, 360000, function () use($position) {
            return App\Slide::where('type', $position)->get();
        });
        return $allSlides;
    }
}

if (! function_exists('postCategories')) {
    function postCategories($clear = false) {
        if ($clear) {
            Cache::forget('postCategories');
        }

        $postCategories = Cache::remember('postCategories', 360000, function () {
            return App\PostCategory::all();
        });
        return $postCategories;
    }
}

if (! function_exists('selectPostCategory')) {
    function selectPostCategory($name, $selected = null) {
        $html = '<select name="'. $name . '">';
        foreach(postCategories() as $cate) {
            $html = $html . '<option value="'. $cate->id . '"' . (($cate->id == $selected) ? ' selected="selected"' : '') .'>' . $cate->name . '</option>';
        }
        $html = $html . '</select>';
        return $html;
    }
}

if (! function_exists('selectGroupCategory')) {
    function selectGroupCategory($name, $selected = null, $blank = false) {
        $html = '<select name="'. $name . '">';
        if ($blank) {
            $html = $html . '<option value=""> --Select category--</option>';
        }
        foreach(treeNav() as $cate) {
            $html = $html . '<optgroup label="'. $cate['name'] .'">';
            foreach($cate['childs'] as $cate_child) {
                $html = $html . '<option value="'. $cate_child['id'] . '"' . (($cate_child['id'] == $selected) ? ' selected="selected"' : '') .'>' . $cate_child['name'] . '</option>';
            }
            $html = $html . '</optgroup>';
        }
        $html = $html . '</select>';
        return $html;
    }
}

if (! function_exists('newsList')) {
    function newsList($limit = 5, $clear = false) {
        if ($clear) {
            Cache::forget('newsList'. $limit);
        }
        $newsList = Cache::remember('newsList'. $limit, 360000, function () use($limit) {
            return App\Post::orderBy('updated_at', 'desc')->offset(0)->limit($limit)->get();
        });
        return $newsList;
    }
}

if (! function_exists('newestPostSameCate')) {
    function newestPostSameCate($pcate_id, $limit = 5) {
        return App\Post::where('post_category_id', $pcate_id)->orderBy('updated_at', 'desc')->offset(0)->limit($limit)->get();
    }
}


if (! function_exists('formatPrice')) {
    function formatPrice($price) {
       if (empty($price) || $price <= 0) return 'Liên hệ';
       return $price;
    }
}


if (! function_exists('categoriesWithLevel')) {
    function categoriesWithLevel($level = 0) {
        return App\Category::where('parent_id', $level)->get();
    }
}