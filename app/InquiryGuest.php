<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InquiryGuest extends Model
{
	protected $table = 'inquiry_guest';

	protected $fillable = [
        'name', 'tel', 'fax', 'address', 'email', 'content', 'is_sent'
    ];

    public function rules() {
        return [
            'inquiry.name' => 'required|max:100',
            'inquiry.email' => 'required|email',
            'inquiry.tel' => 'required',
            'inquiry.captcha' => 'required|captcha',
            'inquiry.content' => 'required'
        ];
    }
}
