<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rules($id = null) {
        return [
            'user.name' => 'required|max:50|unique:users,name,'. $id,
            'user.email' => 'required|email|unique:users,email,'.$id,
            'user.password' => 'required|confirmed'
        ];
    }
}
