<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
	protected $fillable = [
        'title', 'description', 'type', 'url'
    ];

    public function rules($id = null) {
        return [
            'slide.title' => 'required|max:100|unique:slides,title,'. $id,
            'slide.url' => 'image|max:1024' .  (is_null($id) ? '|required' : ''),
            'slide.description' => 'max:150',
            'slide.type' => 'required'
        ];
    }
}
