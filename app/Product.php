<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

	protected $fillable = [
        'name', 'short_des', 'price', 'category_id', 'seo_title', 'seo_description', 'url', 'slug'
    ];

    public function rules($id = null) {
        return [
            'product.name' => 'required|max:200|unique:products,name,'. $id,
            'product.url' => 'image|max:1024' .  (is_null($id) ? '|required' : ''),
            'product.short_des' => 'required|max:255',
            'product.price' => 'numeric',
            'product.category_id' => 'required|numeric',
            'product.seo_title' => 'max:60',
            'product.seo_description' => 'max:158'
        ];
    }

    public function category() {
    	return $this->belongsTo('App\Category');
    }

    public function img($mode = 'thumbnail') {
    	return 'image/products/'. $this->id . '/' . $mode . '/' . $this->url;
    }

    public function lastestProduct($limit = 9, $orderBy = 'updated_at') {
        return $this->orderBy($orderBy, 'desc')->offset(0)->limit($limit)->get();
    }
}
