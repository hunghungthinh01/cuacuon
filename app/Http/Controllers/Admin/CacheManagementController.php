<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CacheManagementController extends Controller
{
    private $arr_caches = [
            'treeNav' => 'Category Left Side Bar',
            'allPartners' => ' Partner List at Left Side Bar',
            'allAds' => 'Ads list at right Side Bar',
            'allServices' => 'Service Customer',
            'allSettings' => 'Setting Basic',
            'allSlides' => 'Slider on Top',
            'postCategories' => 'Category of Post',
            'newsList' => 'Lastest News'
        ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.caches.index', ['caches' => $this->arr_caches]);
    }

    public function clear_cache(Request $request) {
        foreach($request->input('caches') as $cache) {
            if ($cache == 'newsList') {
                call_user_func_array($cache, [5, true]);
            } else {
                call_user_func($cache, true);
            }
        }
        return redirect()->route('caches.index')->with('alert-success','Clear cache was created success!');
    }
}
