<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Service\UploadImage;

class CategoryController extends Controller
{
    use UploadImage;

    protected $category;

    public function __construct(Category $category) {
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::orderBy('parent_id')->orderBy('order');
        if ($request->input('cname')) {
            $categories = $categories->where('name', 'like', $request->input('cname').'%');
        }
        if ($request->input('cparent_id')) {
            $categories = $categories->where('cparent_id', $request->input('cparent_id'));
        }
        $categories = $categories->get();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create', ['category' => $this->category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->category->rules());
        // process store image
        //$name = $this->storeImage($request->file('category.avatar'), 'category');
        Category::create($request->input('category'));
        return redirect()->route('categories.index')->with('alert-success','category was created success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->category->rules($id));
        $category = Category::find($id);
        //
        $category->name = $request->input('category.name');
        $category->seo_title = $request->input('category.seo_title');
        $category->seo_description = $request->input('category.seo_description');
        $category->slug = $request->input('category.slug');
        $category->parent_id = $request->input('category.parent_id');
        $category->order = $request->input('category.order');
        $category->save();
        //
        return redirect()->route('categories.index')->with('alert-success','category was updated success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if($category->parent_id != 0) {
            $category->delete();
        }
        
        return redirect()->route('categories.index')->with('alert-success','category was deleted success!');
    }

    public function clear_cache() {
        treeNav(true);
        return redirect()->route('categories.index')->with('alert-success','Clear cache was created success!');
    }
}
