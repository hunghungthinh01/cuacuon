<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ad;
use App\Service\UploadImage;

class AdController extends Controller
{
    use UploadImage;

    protected $ad;

    public function __construct(Ad $ad) {
        $this->ad = $ad;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::orderBy('order')->get();

        return view('admin.ads.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ads.create', ['ad' => $this->ad]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->ad->rules());
        // process store image
        $name = $this->storeImage($request->file('ad.avatar'), 'ads');
        Ad::create([
            'title' => $request->input('ad.title'),
            'avatar' => $name,
            'redirect_to' => $request->input('ad.redirect_to'),
            'order' => $request->input('ad.order')
        ]);
        return redirect()->route('ads.index')->with('alert-success','Ad was created success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = Ad::find($id);
        return view('admin.ads.edit', compact('ad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->ad->rules($id));
        $ad = Ad::find($id);
        // process store image
        if( $request->hasFile('ad.avatar')) {
            $this->deleteImage($ad->avatar);
            $ad->avatar = $this->storeImage($request->file('ad.avatar'), 'ads');
        }
        //
        $ad->title = $request->input('ad.title');
        $ad->redirect_to = $request->input('ad.redirect_to');
        $ad->order = $request->input('ad.order');
        $ad->save();
        //
        return redirect()->route('ads.index')->with('alert-success','ad was updated success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Ad::find($id);
        $this->deleteImage($ad->avatar);
        $ad->delete();
        
        return redirect()->route('ads.index')->with('alert-success','Partner was deleted success!');
    }

    public function clear_cache(Request $request) {
        allAds(true);
        return redirect()->route('ads.index')->with('alert-success','Clear cache was created success!');
    }
}
