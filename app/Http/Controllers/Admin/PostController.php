<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;

class postController extends Controller
{
    protected $post;

    public function __construct(Post $post) {
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('slug')->paginate(20);
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create', ['post' => $this->post]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->post->rules());
        $post = Post::create($request->input('post'));
        if( $request->hasFile('post.avatar')) {
            $name = $this->storeImagePackage($request->file('post.avatar'), 'posts', $post);
            $post->avatar = $name;
            $post->save();
        }
        return redirect()->route('posts.index')->with('alert-success','post was created success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('admin.posts.edit', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->post->rules($id));
        $post = Post::find($id);
        // process store image
        if( $request->hasFile('post.avatar')) {
            $this->deleteImagePackage('posts', $post);
            $post->avatar = $this->storeImagePackage($request->file('post.url'), 'posts', $post);
        }
        //
        $post->title = $request->input('post.title');
        $post->slug = $request->input('post.slug');
        $post->description = $request->input('post.description');
        $post->post_category_id = $request->input('post.post_category_id');
        $post->seo_title = $request->input('post.seo_title');
        $post->seo_description = $request->input('post.seo_description');
        $post->save();
        //
        return redirect()->route('posts.index')->with('alert-success','post was updated success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $this->deleteImagePackage('posts', $post);
        $post->delete();
        
        return redirect()->route('posts.index')->with('alert-success','Post was deleted success!');
    }
}
