<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Service\UploadImage;

class ProductController extends Controller
{
    use UploadImage;

    protected $product;

    public function __construct(Product $product) {
        $this->product = $product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::with('category')->orderBy('updated_at', 'desc');
        if($request->input('name')) {
            $products = $products->where('name','like', $request->input('name').'%');
        }

        if($request->input('category_id')) {
            $products = $products->where('category_id', $request->input('category_id'));
        }
        $products = $products->paginate(20);
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create', ['product' => $this->product]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->product->rules());
        $product = Product::create($request->input('product'));
        $name = $this->storeImagePackage($request->file('product.url'), 'products', $product);
        $product->url = $name;
        $product->save();
        //
        return redirect()->route('products.index')->with('alert-success','Product was created success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->product->rules($id));
        $product = Product::find($id);
        // process store image
        if( $request->hasFile('product.url')) {
            $this->deleteImagePackage('products', $product);
            $product->url = $this->storeImagePackage($request->file('product.url'), 'products', $product);
        }
        //
        $product->name = $request->input('product.name');
        $product->price = $request->input('product.price');
        $product->category_id = $request->input('product.category_id');
        $product->short_des = $request->input('product.short_des');
        $product->seo_title = $request->input('product.seo_title');
        $product->seo_description = $request->input('product.seo_description');
        $product->save();
        //
        return redirect()->route('products.index')->with('alert-success','Product was updated success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $this->deleteImagePackage('products', $product);
        $product->delete();
        
        return redirect()->route('products.index')->with('alert-success','Product was deleted success!');
    }
}
