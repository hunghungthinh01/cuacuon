<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Partner;
use App\Service\UploadImage;

class PartnerController extends Controller
{
    use UploadImage;

    protected $partner;

    public function __construct(Partner $partner) {
        $this->partner = $partner;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::orderBy('order')->get();

        return view('admin.partners.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partners.create', ['partner' => $this->partner]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->partner->rules());
        // process store image
        $name = $this->storeImage($request->file('partner.avatar'), 'partner');
        Partner::create([
            'partner_name' => $request->input('partner.partner_name'),
            'avatar' => $name,
            'redirect_to' => $request->input('partner.redirect_to'),
            'order' => $request->input('partner.order')
        ]);
        return redirect()->route('partners.index')->with('alert-success','Partner was created success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = Partner::find($id);
        return view('admin.partners.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->partner->rules($id));
        $partner = Partner::find($id);
        // process store image
        if( $request->hasFile('partner.avatar')) {
            $this->deleteImage($partner->avatar);
            $partner->avatar = $this->storeImage($request->file('partner.avatar'), 'partner');
        }
        //
        $partner->partner_name = $request->input('partner.partner_name');
        $partner->redirect_to = $request->input('partner.redirect_to');
        $partner->order = $request->input('partner.order');
        $partner->save();
        //
        return redirect()->route('partners.index')->with('alert-success','Partner was updated success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = Partner::find($id);
        $this->deleteImage($partner->avatar);
        $partner->delete();
        
        return redirect()->route('partners.index')->with('alert-success','Partner was deleted success!');
    }

    public function clear_cache() {
        allPartners(true);
        return redirect()->route('partners.index')->with('alert-success','Clear cache was created success!');
    }
}
