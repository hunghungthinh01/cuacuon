<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;

class PageController extends Controller
{
    protected $page;

    public function __construct(Page $page) {
        $this->page = $page;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::orderBy('slug')->paginate(20);
        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create', ['page' => $this->page]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->page->rules());
        // process store image
        //$name = $this->storeImage($request->file('partner.avatar'), 'partner');
        Page::create([
            'title' => $request->input('page.title'),
            'description' => $request->input('page.description'),
            'seo_title' => $request->input('page.seo_title'),
            'seo_description' => $request->input('page.seo_description')
        ]);
        return redirect()->route('pages.index')->with('alert-success','Page was created success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::find($id);
        return view('admin.pages.edit', ['page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->page->rules($id));
        $page = Page::find($id);
        $page->title = $request->input('page.title');
        $page->description = $request->input('page.description');
        $page->seo_title = $request->input('page.seo_title');
        $page->seo_description = $request->input('page.seo_description');
        $page->save();
        //
        return redirect()->route('pages.index')->with('alert-success','Page was updated success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
