<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ConfigCommon;
use App\Service\UploadImage;

class SettingController extends Controller
{
	use UploadImage;

	protected $data;

	public function __construct(ConfigCommon $setting) {
		$this->data = ConfigCommon::where('code', 'COM')->first();
	}

    public function index() {
    	return view('admin.setting.index', ['data' => unserialize($this->data->value)]);
    }

    public function store(Request $request) {
        $request->validate($this->data->rules());
        $setting = [
            'comp_name' => $request->input('setting.comp_name'),
            'comp_addr' => $request->input('setting.comp_addr'),
            'comp_tel' => $request->input('setting.comp_tel'),
            'comp_fax' => $request->input('setting.comp_fax'),
            'comp_hotline' => $request->input('setting.comp_hotline'),
            'comp_email' => $request->input('setting.comp_email'),
            'comp_website' => $request->input('setting.comp_website'),
            'comp_receive_notify' => $request->input('setting.comp_receive_notify')
        ];
        // process store image
        if( $request->hasFile('setting.header_top')) {
            //$this->deleteImage($partner->avatar);
            $name = $this->storeImage($request->file('setting.header_top'), 'header_top');
            $setting['header_top'] = $name;
        } else {
            $origin_value = unserialize($this->data->value);
            $setting['header_top'] = $origin_value['header_top'];
        }
        $this->data->value = serialize($setting);
        $this->data->save();
        //
        return redirect()->route('setting.index')->with('alert-success','Setting was created success!');
    }

    public function clear_cache() {
        allSettings(true);
        return redirect()->route('setting.index')->with('alert-success','Clear cache was created success!');
    }
}
