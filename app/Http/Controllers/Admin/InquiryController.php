<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\InquiryGuest;

class InquiryController extends Controller
{
	public function index() {
		$inquiries = InquiryGuest::orderBy('updated_at', 'desc')->paginate(20);
		return view('admin.inquiry.index', ['inquiries' => $inquiries]);
	}
}