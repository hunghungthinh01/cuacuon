<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slide;
use App\Service\UploadImage;

class SlideController extends Controller
{
    use UploadImage;

    protected $slide;

    public function __construct(Slide $slide) {
        $this->slide = $slide;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::orderBy('type')->get();

        return view('admin.slides.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slides.create', ['slide' => $this->slide]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->slide->rules());
        // process store image
        $name = $this->storeImage($request->file('slide.url'), 'slide');
        Slide::create([
            'title' => $request->input('slide.title'),
            'url' => $name,
            'description' => $request->input('slide.description'),
            'type' => $request->input('slide.type')
        ]);
        return redirect()->route('slides.index')->with('alert-success','slide was created success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slide::find($id);
        return view('admin.slides.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->slide->rules($id));
        $slide = Slide::find($id);
        // process store image
        if( $request->hasFile('slide.url')) {
            $this->deleteImage($slide->url);
            $slide->url = $this->storeImage($request->file('slide.url'), 'slide');
        }
        //
        $slide->title = $request->input('slide.title');
        $slide->description = $request->input('slide.description');
        $slide->type = $request->input('slide.type');
        $slide->save();
        //
        return redirect()->route('slides.index')->with('alert-success','slide was updated success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);
        $this->deleteImage($slide->url);
        $slide->delete();
        
        return redirect()->route('slides.index')->with('alert-success','slide was deleted success!');
    }

    public function clear_cache() {
        allSlides('top', true);
        allSlides('bottom', true);
        return redirect()->route('slides.index')->with('alert-success','Clear cache was created success!');
    }
}
