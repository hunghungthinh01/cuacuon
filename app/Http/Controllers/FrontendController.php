<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\PostCategory;
use App\Page;
use App\Post;
use App\InquiryGuest;

class FrontendController extends Controller
{
    protected $category;

    public function __construct(Category $category) {
        $this->category = $category;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $product = new Product();
        return view('home.index', ['products' => $product->lastestProduct()]);
    }

    public function category(Request $request, $slug = null) {
        $product_sql = Product::with('category');
        if (!empty($slug)) {
            $this->category = Category::where('slug', $slug)->first();
            if (is_null($this->category)) {
                $products = [];
            } else {
                $products = $product_sql->where('category_id', $this->category->id);
            }
        } else {
            $this->category = new Category(['name' => 'Tất cả sản phẩm', 'slug' => '']);
            $products = $product_sql->orderBy('name');
        }
        if ($products != []) $products = $products->paginate(15);
        return view('home.products', ['products'=> $products, 'category' => $this->category]);
    }

    private function get_data_products($request = [], $slug = 0) {

        if ($slug) {
            $this->category = Category::where('slug', $slug)->first();

            if (is_null($this->category)) return [];

            return Product::with('category')->where('category_id', $this->category->id)->get();
        }

        return Product::with('category')->orderBy('category_id')->get();
    }

    public function product(Request $request, $slug) {
        $product = Product::where('slug', $slug)->first();
        $sameProducts = Product::where('category_id', $product->category_id)
                                ->where('id', '!=', $product->id)
                                ->offset(0)->limit(9)->get();
        return view('home.product_detail', ['product' => $product, 'sameProducts' => $sameProducts]);
    }

    public function pCategory(Request $request, $slug) {
        $pcate = PostCategory::where('slug', $slug)->first();
        return view('home.post_category', ['pcate' => $pcate]);
    }

    public function postDetail(Request $request, $slug) {
        $post = Post::where('slug', $slug)->first();
        return view('home.post_detail', ['post' => $post]);
    }

    public function pageDetail(Request $request, $slug) {
        $page = Page::where('slug', $slug)->first();
        return view('home.page_detail', ['page' => $page]);
    }

    public function getInquiry() {
        return view('home.inquiry');
    }

    public function postInquiry(Request $request) {
        $ad = new \App\InquiryGuest();
        $request->validate($ad->rules());

        InquiryGuest::create([
            'name' => $request->input('inquiry.name'),
            'address' => $request->input('inquiry.address'),
            'email' => $request->input('inquiry.email'),
            'tel' => $request->input('inquiry.tel'),
            'fax' => $request->input('inquiry.fax'),
            'content' => $request->input('inquiry.content')
        ]);
        return redirect()->route('get_inquiry')->with('alert-success','Sent was success!');
    }
}
