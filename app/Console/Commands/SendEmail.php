<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\NotifyEmail;
use App\InquiryGuest;
use Illuminate\Support\Facades\Mail;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendEmail:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email when has income inquiry';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target_email = 'hunghungthinh01@gmail.com';
        if (
            array_key_exists("comp_receive_notify", allSettings()) 
            && allSettings()['comp_receive_notify']
        ) {
            $target_email = allSettings()['comp_receive_notify'];
        }

        $inquiries = InquiryGuest::where('is_sent', false)->get();
        foreach ($inquiries as $new_ingoing) {
            Mail::to($target_email)->send(new NotifyEmail($new_ingoing));
            $new_ingoing->is_sent = true;
            $new_ingoing->save();
        }
    }
}
