<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use Sluggable;

    protected $fillable = [
        'name', 'order', 'avatar', 'parent_id', 'slug', 'seo_title', 'seo_description'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function rules($id = null) {
        return [
            'category.name' => 'required|max:150|unique:categories,name,'. $id,
            'category.parent_id' => 'required',
            'category.seo_description' => 'max:158',
            'category.seo_title' => 'max:60',
            'category.slug' => 'max:150'
        ];
    }

    public static function treeNav() {
    	$results = [];
    	foreach(Category::orderBy('parent_id')->orderBy('order')->get() as $node) {
    		if ($node->parent_id == 0) {
    			$results[$node->id] = [
					'name' => $node->name,
					'id' => $node->id,
					'parent_id' => $node->parent_id,
                    'slug' => $node->slug,
					'childs' => []
    			];
    			continue;
    		}
    		$results[$node->parent_id]['childs'][$node->id] = [
				'name' => $node->name,
				'id' => $node->id,
				'parent_id' => $node->parent_id,
                'slug' => $node->slug

    		];
    	}
    	return $results;
    }

    public function parent() {
        return $this->hasOne('\App\Category', 'id', 'parent_id');
    }
}
