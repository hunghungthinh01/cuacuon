<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Page extends Model
{
    use Sluggable;

    protected $fillable = [
        'title', 'description', 'slug', 'avatar', 'seo_title', 'seo_description'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function rules($id = null) {
        return [
            'page.title' => 'required|max:150|unique:pages,title,'. $id,
            'page.avatar' => 'image|max:1024' .  (is_null($id) ? '' : ''),
            'page.description' => 'required'
        ];
    }
}
