<?php

use Illuminate\Database\Seeder;
use App\Partner;

class PartnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Partner::create([
        	'partner_name' => 'Austdoor',
        	'avatar' => 'image/partner/Austdoor.PNG',
        	'redirect_to' => '#',
        	'order' => 1
        ]);

        Partner::create([
        	'partner_name' => 'HL Glass',
        	'avatar' => 'image/partner/HL glass.PNG',
        	'redirect_to' => '#',
        	'order' => 2
        ]);

        Partner::create([
        	'partner_name' => 'Masterdoor',
        	'avatar' => 'image/partner/Masterdoor.PNG',
        	'redirect_to' => '#',
        	'order' => 3
        ]);

        Partner::create([
        	'partner_name' => 'Sinil',
        	'avatar' => 'image/partner/Sinil.PNG',
        	'redirect_to' => '#',
        	'order' => 4
        ]);

        Partner::create([
        	'partner_name' => 'WP',
        	'avatar' => 'image/partner/WP.PNG',
        	'redirect_to' => '#',
        	'order' => 5
        ]);
    }
}
