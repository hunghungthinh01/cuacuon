<?php

use Illuminate\Database\Seeder;
use App\ConfigCommon;

class ConfigCommonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ConfigCommon::create([
        	'code' => 'COM',
        	'value'=> ''
        ]);
    }
}
