<?php

use Illuminate\Database\Seeder;
use App\PostCategory;

class PostCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostCategory::create([
        	'name' => 'Tin tức xây dựng',
            'slug' => 'tin-tuc-xay-dung'
        ]);

        PostCategory::create([
        	'name' => 'Thông tin thị trường',
            'slug' => 'thong-tin-thi-truong'
        ]);

        PostCategory::create([
        	'name' => 'Tin từ doanh nghiệp',
            'slug' => 'tin-tu-doanh-nghiep'
        ]);

        PostCategory::create([
            'name' => 'Tư vấn kỹ thuật lắp đặt',
            'slug' => 'tu-van-ky-thuat-lap-dat'
        ]);

        PostCategory::create([
            'name' => 'Hướng dẫn sử dụng',
            'slug' => 'huong-dan-su-dung'
        ]);

        PostCategory::create([
            'name' => 'Tư vấn phong thủy',
            'slug' => 'tu-van-phong-thuy'
        ]);

        PostCategory::create([
            'name' => 'Tài liệu',
            'slug' => 'tai-lieu'
        ]);

        PostCategory::create([
            'name' => 'Cẩm nang nhà đẹp',
            'slug' => 'cam-nang-nha-dep'
        ]);

        PostCategory::create([
            'name' => 'Cẩm nang xây dựng',
            'slug' => 'cam-nang-xay-dung'
        ]);

        PostCategory::create([
            'name' => 'Loai Khac',
            'slug' => 'loai-khac'
        ]);
    }
}
