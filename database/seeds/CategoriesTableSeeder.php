<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cate1 = Category::create([
        	'name' => 'Cửa Nhôm',
        	'parent_id' => 0,
        	'order' => 1,
            'slug' => 'cua-nhom'
        ]);
        Category::create([
        	'name' => 'Nhôm Hệ',
        	'parent_id' => $cate1->id,
        	'order' => 11,
            'slug' => 'nhom-he'
        ]);
        Category::create([
        	'name' => 'Vách Mặt Dựng',
        	'parent_id' => $cate1->id,
        	'order' => 12,
            'slug' => 'vach-mat-dung'
        ]);
        //---------------------
        // 2
        $cate2 = Category::create([
        	'name' => 'Cửa Kính',
        	'parent_id' => 0,
        	'order' => 1,
            'slug' => 'cua-kinh'
        ]);
        Category::create([
        	'name' => 'Cửa Kính Thủy Lực',
        	'parent_id' => $cate2->id,
        	'order' => 22,
            'slug' => 'cua-kinh-thuy-luc'
        ]);
        Category::create([
        	'name' => 'Cửa Tự Động',
        	'parent_id' => $cate2->id,
        	'order' => 23,
            'slug' => 'cua-tu-dong'
        ]);
        Category::create([
        	'name' => 'Phụ Kiện Cửa Kính',
        	'parent_id' => $cate2->id,
        	'order' => 24,
            'slug' => 'phu-kien-cua-kinh'
        ]);
        //----------------------
        // 3
        $cate3 = Category::create([
        	'name' => 'Cửa Nhựa Lõi Thép',
        	'parent_id' => 0,
        	'order' => 3,
            'slug' => 'cua-nhua-loi-thep'
        ]);
        Category::create([
        	'name' => 'Cửa Đi Chính',
        	'parent_id' => $cate3->id,
        	'order' => 31,
            'slug' => 'cua-di-chinh'
        ]);
        Category::create([
        	'name' => 'Cửa Sổ Mở Ngoài',
        	'parent_id' => $cate3->id,
        	'order' => 32,
            'slug' => 'cua-so-mo-ngoai'
        ]);
        Category::create([
        	'name' => 'Cửa Đi Balcon',
        	'parent_id' => $cate3->id,
        	'order' => 33,
            'slug' => 'cua-di-balcon'
        ]);
        Category::create([
        	'name' => 'Cửa Đi Thông Phòng',
        	'parent_id' => $cate3->id,
        	'order' => 34,
            'slug' => 'cua-di-thong-phong'
        ]);
        Category::create([
        	'name' => 'Vách Kính',
        	'parent_id' => $cate3->id,
        	'order' => 35,
            'slug' => 'vach-kinh'
        ]);
        Category::create([
        	'name' => 'Hệ Khóa',
        	'parent_id' => $cate3->id,
        	'order' => 36,
            'slug' => 'he-khoa'
        ]);
        //----------------------
        // 4
        $cate4 = Category::create([
        	'name' => 'Cửa Cuốn',
        	'parent_id' => 0,
        	'order' => 4,
            'slug' => 'cua-cuon'
        ]);
        Category::create([
        	'name' => 'Cửa Khe Thoáng',
        	'parent_id' => $cate4->id,
        	'order' => 41,
            'slug' => 'cua-khe-thoang'
        ]);
        Category::create([
        	'name' => 'Cửa Tấm Liền',
        	'parent_id' => $cate4->id,
        	'order' => 43,
            'slug' => 'cua-tam-lien'
        ]);
        Category::create([
        	'name' => 'Motor, phụ kiện cửa cuốn',
        	'parent_id' => $cate4->id,
        	'order' => 43,
            'slug' => 'motor-phu-kien-cua-cuon'
        ]);
        //----------------------
        // 5
        $cate5 = Category::create([
        	'name' => 'Cổng',
        	'parent_id' => 0,
        	'order' => 5,
            'slug' => 'cong'
        ]);
        Category::create([
        	'name' => 'Cổng Nhôm Đúc',
        	'parent_id' => $cate5->id,
        	'order' => 51,
            'slug' => 'cong-nhom-duc'
        ]);
        Category::create([
        	'name' => 'Cổng Sắt Mỹ Thuật',
        	'parent_id' => $cate5->id,
        	'order' => 52,
            'slug' => 'cong-sat-my-thuat'
        ]);
        Category::create([
        	'name' => 'Cổng Inox Tự Động',
        	'parent_id' => $cate5->id,
        	'order' => 53,
            'slug' => 'cong-inox-tu-dong'
        ]);
        //----------------------
        //6
        $cate6 = Category::create([
        	'name' => 'Sơn Nội - Ngoại Thất',
        	'parent_id' => 0,
        	'order' => 6,
            'slug' => 'son-noi-ngoai-that'
        ]);
        Category::create([
        	'name' => 'Sơn Nội Thất',
        	'parent_id' => $cate6->id,
        	'order' => 61,
            'slug' => 'son-noi-that'
        ]);
        Category::create([
        	'name' => 'Sơn Ngoại Thất',
        	'parent_id' => $cate6->id,
        	'order' => 62,
            'slug' => 'son-ngoai-that'
        ]);
        Category::create([
        	'name' => 'Bột Trét Tường',
        	'parent_id' => $cate6->id,
        	'order' => 63,
            'slug' => 'bot-tret-tuong'
        ]);
        //----------------------
        // 7
        $cate7 = Category::create([
        	'name' => 'Gạch Ceramic',
        	'parent_id' => 0,
        	'order' => 7,
            'slug' => 'gach-ceramic'
        ]);
        Category::create([
        	'name' => 'Gạch Lát Nền',
        	'parent_id' => $cate7->id,
        	'order' => 71,
            'slug' => 'gach-lat-nen'
        ]);
        Category::create([
        	'name' => 'Gạch Ốp Tường',
        	'parent_id' => $cate7->id,
        	'order' => 72,
            'slug' => 'gach-op-tuong'
        ]);
        Category::create([
        	'name' => 'Gạch Chân Tường',
        	'parent_id' => $cate7->id,
        	'order' => 73,
            'slug' => 'gach-chan-tuong'
        ]);
        Category::create([
        	'name' => 'Gạch Trang Trí',
        	'parent_id' => $cate7->id,
        	'order' => 74,
            'slug' => 'gach-trang-tri'
        ]);
        //----------------------
        // 8
        $cate8 = Category::create([
        	'name' => 'Thiết Bị Phòng Tắm',
        	'parent_id' => 0,
        	'order' => 8,
            'slug' => 'thiet-bi-phong-tam'
        ]);
        Category::create([
        	'name' => 'Thiết Bị Vệ Sinh',
        	'parent_id' => $cate8->id,
        	'order' => 81,
            'slug' => 'thiet-bi-ve-sinh'
        ]);
        Category::create([
        	'name' => 'Phụ Kiện Phòng Tắm',
        	'parent_id' => $cate8->id,
        	'order' => 82,
            'slug' => 'phu-kien-phong-tam'
        ]);
        Category::create([
        	'name' => 'Bình Nước Nóng',
        	'parent_id' => $cate8->id,
        	'order' => 83,
            'slug' => 'binh-nuoc-nong'
        ]);
        //----------------------
        // 9
        $cate9 = Category::create([
        	'name' => 'Sản Phẩm Khác',
        	'parent_id' => 0,
        	'order' => 9,
            'slug' => 'san-pham-khac'
        ]);
        Category::create([
        	'name' => 'Cầu Thánh Kính',
        	'parent_id' => $cate9->id,
        	'order' => 91,
            'slug' => 'cau-thang-chinh'
        ]);
        Category::create([
        	'name' => 'Lan Can Inox Kính',
        	'parent_id' => $cate9->id,
        	'order' => 92,
            'slug' => 'lan-can-inox-kinh'
        ]);
        Category::create([
        	'name' => 'Sản Phẩm Khác',
        	'parent_id' => $cate9->id,
        	'order' => 93,
            'slug' => 'san-pham-khac'
        ]);
        //----------------------
    }
}
