<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(ConfigCommonsTableSeeder::class);
        $this->call(PostCategoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PartnersTableSeeder::class);
    }
}
