<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'quyennx',
        	'email' => 'quyennx@gmail.com',
        	'password' => \Hash::make('123456'),
        	//'password_confirmation' => '123456'
        ]);

        User::create([
        	'name' => 'hungn',
        	'email' => 'hunghungthinh01@gmail.com',
        	'password' => \Hash::make('654321'),
        	//'password_confirmation' => '654321'
        ]);
    }
}
