<?php
// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Trang chủ', route('frontend_root'));
});

// Home > Category
Breadcrumbs::for('post_category', function ($trail, $post_category) {
    $trail->parent('home');
    $trail->push($post_category->name, route('post_category_home', $post_category->slug));
});
// Home > Category > Post
Breadcrumbs::for('post', function ($trail, $post) {
    $trail->parent('home');
    $trail->push($post->post_category->name, route('post_category_home', $post->post_category->slug));
    $trail->push($post->title, route('post_detail_home', $post->slug));
});

// Home > Page
Breadcrumbs::for('page', function ($trail, $page) {
    $trail->parent('home');
    $trail->push($page->title, route('page_detail_home', $page->slug));
});

// Home > Category Product
Breadcrumbs::for('product_category', function ($trail, $pcate) {
    $trail->parent('home');
    $trail->push($pcate->name, route('post_category_home', $pcate->slug));
});
// Home > Cate Product > Product
Breadcrumbs::for('product', function ($trail, $product) {
    $trail->parent('home');
    $trail->push($product->category->name, route('category_home', $product->category->slug));
    $trail->push($product->name, route('product_home', $product->slug));
});

// Home > Liên hệ
Breadcrumbs::for('contact_us', function ($trail) {
    $trail->parent('home');
    $trail->push('Liên hệ', route('frontend_root'));
});
