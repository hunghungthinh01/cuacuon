<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index')->name('frontend_root');

Route::group(['prefix' => 'ql'], function() {
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	//
	Route::post('login', 'Auth\LoginController@login');
});

Route::group(['prefix' => 'ql', 'middleware' => 'auth'], function() {
	
	Route::get('/', 'Admin\AccountController@index')->name('admin_default_root');
	//
	Route::get('logout', 'Auth\LoginController@logout')->name('logout');
	// admin managemening users
	Route::resource('accounts', 'Admin\AccountController');
	// partner
	Route::get('partners/clear_cache', 'Admin\PartnerController@clear_cache')->name('partners.clear_cache');

	Route::resource('partners', 'Admin\PartnerController');

	Route::get('categories/clear_cache', 'Admin\CategoryController@clear_cache')->name('categories.clear_cache');

	Route::resource('categories', 'Admin\CategoryController');
	//
	Route::resource('products', 'Admin\ProductController');

	Route::resource('pages', 'Admin\PageController');

	Route::resource('posts', 'Admin\PostController');

	Route::get('ads/clear_cache', 'Admin\AdController@clear_cache')->name('ads.clear_cache');

	Route::resource('ads', 'Admin\AdController');
	
	Route::get('slides/clear_cache', 'Admin\SlideController@clear_cache')->name('slides.clear_cache');

	Route::resource('slides', 'Admin\SlideController');

	Route::get('setting', 'Admin\SettingController@index')->name('setting.index');

	Route::get('setting/clear_cache', 'Admin\SettingController@clear_cache')->name('setting.clear_cache');

	Route::post('setting', 'Admin\SettingController@store');

	Route::get('inquiry/index', 'Admin\InquiryController@index')->name('inquiry.index');

	Route::post('caches/clear_cache', 'Admin\CacheManagementController@clear_cache')->name('caches.clear_cache');

	Route::get('caches/index', 'Admin\CacheManagementController@index')->name('caches.index');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/product-category/{slug?}', 'FrontendController@category')->name('category_home');

Route::get('/product/{slug}', 'FrontendController@product')->name('product_home');

Route::get('/post-category/{slug}', 'FrontendController@pCategory')->name('post_category_home');

Route::get('/news/{slug}', 'FrontendController@postDetail')->name('post_detail_home');

Route::get('/pages/{slug}', 'FrontendController@pageDetail')->name('page_detail_home');

Route::get('/inquiry', 'FrontendController@getInquiry')->name('get_inquiry');

Route::post('/inquiry', 'FrontendController@postInquiry')->name('post_inquiry');

 Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });
