<?php

return [

    'news' => [
        '1' => 'Tin tức từ Navico',
        '2' => 'Tin tức xây dựng',
        '3' => 'Thông tin thị trường',
        '4' => 'Tư vấn kỹ thuật lắp đặt',
        '5' => 'Hướng dẫn sử dụng',
        '6' => 'Tư vấn phong thủy',
        '7' => 'Cẩm nang nhà đẹp',
        '8' => 'Tài liệu',
        '9' => 'Cẩm nang xây dựng',
        '10' => 'Hỏi đáp',
    ],

];
 