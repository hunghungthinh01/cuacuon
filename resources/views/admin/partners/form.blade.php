@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@php($url = is_null($partner->id) ? route('partners.store') : route('partners.update', $partner->id))
<form method="post" action="{{ $url }}" class="form-horizontal" enctype="multipart/form-data">
    <div class="box-body no-padding">
        @csrf
        @if(!is_null($partner->id))
            @method('PATCH')
        @endif
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Partner Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="partner[partner_name]" value="{{ $partner->partner_name }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Avatar</label>
            <div class="col-sm-10">
                <p>Nên format ảnh theo kich thước 236x90px!</p>
                @if (!empty($partner->avatar))
                <img src="{{ asset($partner->avatar) }}" height="100px" width="100px"/>
                @endif
                <input type="file" class="form-control" name="partner[avatar]" accept="image/*"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Link tới đối tác</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="partner[redirect_to]" value="{{ $partner->redirect_to }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Thứ tự hiển thị(0..10)</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="partner[order]" value="{{ $partner->order }}"/>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ route('partners.index') }}" class="btn btn-primary pull-left">Cancel</a>
        <button type="submit" class="btn btn-primary pull-right">{{ is_null($partner->id) ? 'Create' : 'Update' }}</button>
    </div>
</form>