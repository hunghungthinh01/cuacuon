@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Create Partner</h3>
            </div>
            <!-- /.box-header -->
            @include('admin.partners.form')
        </div>
    </div>
</div>
@endsection