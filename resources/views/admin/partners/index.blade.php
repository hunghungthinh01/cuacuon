@extends('layouts.admin')

@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Danh sach doi tac</h3>
            <div class="box-tools">
               <div class="input-group input-group-sm">
                  <a class="label label-success" href="{{route('partners.create')}}">New</a>
                  <a class="label label-success" href="{{ route('partners.clear_cache') }}">Clear Cache</a>
               </div>
            </div>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            @if (empty($partners))
                Khong co tai doi tac nao!
            @else
                <table class="table table-hover">
                   <tbody>
                      <tr>
                         <th>ID</th>
                         <th>Partner Name</th>
                         <th>Avatar</th>
                         <th>Redirect to</th>
                         <th>Order</th>
                         <th></th>
                         <th></th>
                      </tr>
                      @foreach($partners as $partner)
                      <tr>
                         <td>{{ $partner->id }}</td>
                         <td>{{ $partner->partner_name }}</td>
                         <td>
                          <img src="{{ asset($partner->avatar) }}" height="50px" width="50px"/>
                         </td>
                         <td>{{ $partner->redirect_to }}</td>
                         <td>{{ $partner->order }}</td>
                         <td>
                             <a class="label label-primary" href="{{ route('partners.edit', $partner->id) }}">Edit</a>
                         </td>
                         <td>
                             <form action="{{ route('partners.destroy', $partner->id) }}" method="post" class="confirmDeleteRecord">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="label label-danger">Delete</button>
                             </form>
                         </td>
                      </tr>
                      @endforeach
                   </tbody>
                </table>
            @endif
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
</div>
@endsection
