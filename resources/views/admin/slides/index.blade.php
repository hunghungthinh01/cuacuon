@extends('layouts.admin')

@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Danh sach Slides</h3>
            <div class="box-tools">
               <div class="input-group input-group-sm">
                  <a class="label label-success" href="{{route('slides.create')}}">New</a>
                  <a class="label label-success" href="{{ route('slides.clear_cache') }}">Clear Cache</a>
               </div>
            </div>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            @if (empty($slides))
                Khong co slide nao!
            @else
                <table class="table table-hover">
                   <tbody>
                      <tr>
                         <th>ID</th>
                         <th>Title</th>
                         <th>URL</th>
                         <th>Description</th>
                         <th>Type</th>
                         <th></th>
                         <th></th>
                      </tr>
                      @foreach($slides as $slide)
                      <tr>
                         <td>{{ $slide->id }}</td>
                         <td>{{ $slide->title }}</td>
                         <td>
                          <img src="{{ asset($slide->url) }}" width="200px"/>
                         </td>
                         <td>{{ $slide->description }}</td>
                         <td>{{ $slide->type }}</td>
                         <td>
                             <a class="label label-primary" href="{{ route('slides.edit', $slide->id) }}">Edit</a>
                         </td>
                         <td>
                             <form action="{{ route('slides.destroy', $slide->id) }}" method="post" class="confirmDeleteRecord">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="label label-danger">Delete</button>
                             </form>
                         </td>
                      </tr>
                      @endforeach
                   </tbody>
                </table>
            @endif
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
</div>
@endsection
