@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@php($url = is_null($slide->id) ? route('slides.store') : route('slides.update', $slide->id))
<form method="post" action="{{ $url }}" class="form-horizontal" enctype="multipart/form-data">
    <div class="box-body no-padding">
        @csrf
        @if(!is_null($slide->id))
            @method('PATCH')
        @endif
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="slide[title]" value="{{ $slide->title }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">URL</label>
            <div class="col-sm-10">
                <p>Slide TOP: 602x300 px, slide BOTTOM: 182x132px</p>
                @if (!empty($slide->url))
                <img src="{{ asset($slide->url) }}" height="100px" width="100px"/>
                @endif
                <input type="file" class="form-control" name="slide[url]" accept="image/*"/>
            </div>
        </div>

        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Type</label>
            <div class="col-sm-4">
                <select name="slide[type]" class="form-control">
                    <option value="top" {{ $slide->type == 'top' ? 'selected="selected"' : '' }}>Top</option>
                    <option value="bottom" {{ $slide->type == 'bottom' ? 'selected="selected"' : '' }}>Bottom</option>
                </select>
            </div>
            <div class="col-sm-6">
                <span> TOP: 602x300 px</span>
                <br/>
                <span> BOTTOM: 182x132px</span>
            </div>
        </div>

        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="slide[description]" value="{{ $slide->description }}"/>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ route('slides.index') }}" class="btn btn-primary pull-left">Cancel</a>
        <button type="submit" class="btn btn-primary pull-right">{{ is_null($slide->id) ? 'Create' : 'Update' }}</button>
    </div>
</form>