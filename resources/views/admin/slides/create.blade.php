@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Create Slide</h3>
            </div>
            <!-- /.box-header -->
            @include('admin.slides.form')
        </div>
    </div>
</div>
@endsection