@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Create Category</h3>
            </div>
            <!-- /.box-header -->
            @include('admin.categories.form')
        </div>
    </div>
</div>
@endsection