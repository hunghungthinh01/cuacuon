@extends('layouts.admin')

@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Danh sach danh mục sản phẩm</h3>
            <div class="box-tools">
               <div class="input-group input-group-sm">
                  <a class="label label-success" href="{{route('categories.create')}}">New</a>
                  <a class="label label-success" href="{{ route('categories.clear_cache') }}">Clear Cache</a>
               </div>
            </div>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            @if (empty($categories))
                Khong co tai danh muc nao!
            @else
                <table class="table table-hover">
                   <tbody>
                      <tr>
                         <th>ID</th>
                         <th>Name</th>
                         <th>Parent</th>
                         <th>Slug</th>
                         <th>Order</th>
                         <th></th>
                         <th></th>
                      </tr>
                      @foreach($categories as $category)
                      <tr>
                         <td>{{ $category->id }}</td>
                         <td>{{ $category->name }}</td>
                         <td>
                          @if (!empty($category->parent))
                          {{ $category->parent->name }}
                          @else
                           Root(Level 1)
                          @endif
                         </td>
                         <td>{{ $category->slug }}</td>
                         <td>{{ $category->order }}</td>
                         <td>
                             <a class="label label-primary" href="{{ route('categories.edit', $category->id) }}">Edit</a>
                         </td>
                         <td>
                             <form action="{{ route('categories.destroy', $category->id) }}" method="post" class="confirmDeleteRecord">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="label label-danger">Delete</button>
                             </form>
                         </td>
                      </tr>
                      @endforeach
                   </tbody>
                </table>
            @endif
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
</div>
@endsection
