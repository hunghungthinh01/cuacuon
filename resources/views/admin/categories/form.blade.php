@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@php($url = is_null($category->id) ? route('categories.store') : route('categories.update', $category->id))
<form method="post" action="{{ $url }}" class="form-horizontal" enctype="multipart/form-data">
    <div class="box-body no-padding">
        @csrf
        @if(!is_null($category->id))
            @method('PATCH')
        @endif
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="category[name]" value="{{ $category->name ?? old('category.name') }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Slug</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="category[slug]" value="{{ $category->slug ?? old('category.slug') }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Parent_id</label>
            <div class="col-sm-10">
                <select name="category[parent_id]">
                    @if ($category->parent_id == 0 && $category->id)
                    <option value="0">Root(Level 1)</option>
                    @else
                        @foreach(categoriesWithLevel() as $cate_root)
                            <option value="{{ $cate_root->id }}" {{$cate_root->id == $category->parent_id ? 'selected="selected"' : ''}}>{{ $cate_root->name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Thứ tự hiển thị(0..10)</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="category[order]" value="{{ $category->order }}"/>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ route('categories.index') }}" class="btn btn-primary pull-left">Cancel</a>
        <button type="submit" class="btn btn-primary pull-right">{{ is_null($category->id) ? 'Create' : 'Update' }}</button>
    </div>
</form>