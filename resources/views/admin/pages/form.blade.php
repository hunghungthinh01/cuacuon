@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@php($url = is_null($page->id) ? route('pages.store') : route('pages.update', $page->id))
<form method="post" action="{{ $url }}" class="form-horizontal" enctype="multipart/form-data">
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body no-padding">
                @csrf
                @if(!is_null($page->id))
                    @method('PATCH')
                @endif
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="page[title]" value="{{ $page->title }}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Slug</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="page[slug]" value="{{ $page->slug }}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea type="text" class="form-control" name="page[description]" id="testDesId"/>
                            {{ $page->description }}
                        </textarea>
                    </div>
                </div>

<!--                 <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Avatar</label>
                    <div class="col-sm-10">
                        @if (!empty($page->avatar))
                        <img src="{{ asset($page->avatar) }}" height="100px" width="100px"/>
                        @endif
                        <input type="file" class="form-control" name="page[avatar]" accept="image/*"/>
                    </div>
                </div> -->

                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Seo Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="page[seo_title]" value="{{ $page->seo_title }}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Seo Description</label>
                    <div class="col-sm-10">
                        <textarea type="text" class="form-control" name="page[seo_description]"/>
                            {{ $page->seo_description }}
                        </textarea>
                    </div>
                </div>

                <div class="box-footer">
                    <a href="{{ route('pages.index') }}" class="btn btn-primary pull-left">Cancel</a>
                    <button type="submit" class="btn btn-primary pull-right">{{ is_null($page->id) ? 'Create' : 'Update' }}</button>
                </div>
            </div>
        </div>
    </div>
</form>