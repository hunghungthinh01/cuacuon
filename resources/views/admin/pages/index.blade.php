@extends('layouts.admin')

@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Danh sach doi tac</h3>
            <div class="box-tools">
               <div class="input-group input-group-sm">
                  <a class="label label-success" href="{{route('pages.create')}}">New</a>
               </div>
            </div>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            @if (empty($pages))
                Khong co trang nao!
            @else
                <table class="table table-hover">
                   <tbody>
                      <tr>
                         <th>ID</th>
                         <th>Page Name</th>
                         <th>Page Slug</th>
                         <th>Avatar</th>
                         <th>Redirect to</th>
                         <th>Order</th>
                         <th></th>
                         <th></th>
                      </tr>
                      @foreach($pages as $page)
                      <tr>
                         <td>{{ $page->id }}</td>
                         <td>{{ $page->title }}</td>
                         <td>{{ $page->slug }}</td>
                         <td>
                             <a class="label label-primary" href="{{ route('pages.edit', $page->id) }}">Edit</a>
                         </td>
                         <td>
                             <form action="{{ route('pages.destroy', $page->id) }}" method="post" class="confirmDeleteRecord">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="label label-danger">Delete</button>
                             </form>
                         </td>
                      </tr>
                      @endforeach
                   </tbody>
                </table>
            @endif
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <div class="center">{{ $pages->links() }}</div>
   </div>
</div>
@endsection
