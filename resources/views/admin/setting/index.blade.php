@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Setting</h3>
                <div class="box-tools">
                   <div class="input-group input-group-sm">
                      <a class="label label-success" href="{{ route('setting.clear_cache') }}">Clear Cache</a>
                   </div>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="post" action="{{ route('setting.index') }}" class="form-horizontal" enctype="multipart/form-data">
                    <div class="box-body no-padding">
                        @csrf
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Company Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="setting[comp_name]" value="{{ $data['comp_name'] }}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Company Address</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="setting[comp_addr]" value="{{ $data['comp_addr'] }}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Company Tel</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="setting[comp_tel]" value="{{ $data['comp_tel'] }}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Company Fax</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="setting[comp_fax]" value="{{ $data['comp_fax'] }}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Company Hotline</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="setting[comp_hotline]" value="{{ $data['comp_hotline'] }}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Company Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="setting[comp_email]" value="{{ $data['comp_email'] }}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Company Website</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="setting[comp_website]" value="{{ $data['comp_website'] }}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Email nhận thông báo</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="setting[comp_receive_notify]" value="{{ $data['comp_receive_notify'] ?? '' }}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Header Top(1140x150)</label>
                            <div class="col-sm-10">
                                @if (!empty($data['header_top']))
                                <img src="{{ asset($data['header_top']) }}" height="100px"/>
                                @endif
                                <input type="file" class="form-control" name="setting[header_top]" accept="image/*"/>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ route('setting.index') }}" class="btn btn-primary pull-left">Cancel</a>
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection