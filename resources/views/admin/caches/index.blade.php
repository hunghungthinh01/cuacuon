@extends('layouts.admin')

@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Danh sach caches</h3>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
          <form method="post" action="{{ route('caches.clear_cache') }}" class="form-horizontal">
            @csrf
            <table class="table table-hover">
               <tbody>
                  <tr>
                     <th>ID</th>
                     <th>Cache Name</th>
                  </tr>
                  @foreach($caches as $cache_val => $cache_name)
                  <tr>
                     <td>
                      <input type="checkbox" name="caches[]" value="{{ $cache_val }}">
                    </td>
                     <td>{{ $cache_name }}</td>
                  </tr>
                  @endforeach
               </tbody>
            </table>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </div>
          </form>
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
</div>
@endsection
