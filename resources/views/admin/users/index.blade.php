@extends('layouts.admin')

@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Users List</h3>
            <div class="box-tools">
               <div class="input-group input-group-sm">
                  <a class="label label-success" href="{{route('accounts.create')}}">New</a>
               </div>
            </div>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            @if (empty($users))
                Khong co tai khoan nao!
            @else
                <table class="table table-hover">
                   <tbody>
                      <tr>
                         <th>ID</th>
                         <th>UserName</th>
                         <th>Email</th>
                         <th>Created at</th>
                         <th>Action</th>
                         <th></th>
                      </tr>
                      @foreach($users as $user)
                      <tr>
                         <td>{{ $user->id }}</td>
                         <td>{{ $user->name }}</td>
                         <td>{{ $user->email }}</td>
                         <td>{{ $user->created_at }}</td>
                         <td>
                             <a class="label label-primary" href="{{ route('accounts.edit', $user->id) }}">Edit</a>
                         </td>
                         <td>
                             <form action="{{ route('accounts.destroy', $user->id) }}" method="post" class="confirmDeleteRecord">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="label label-danger">Delete</button>
                             </form>
                         </td>
                      </tr>
                      @endforeach
                   </tbody>
                </table>
            @endif
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
</div>
@endsection
