@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@php($url = is_null($user->id) ? route('accounts.store') : route('accounts.update', $user->id))
<form method="post" action="{{ $url }}" class="form-horizontal">
    <div class="box-body no-padding">
        @csrf
        @if(!is_null($user->id))
            @method('PATCH')
        @endif
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="user[name]" value="{{ $user->name }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="user[email]" value="{{ $user->email }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="user[password]"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Password Confirmation</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="user[password_confirmation]"/>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ route('accounts.index') }}" class="btn btn-primary pull-left">Cancel</a>
        <button type="submit" class="btn btn-primary pull-right">{{ is_null($user->id) ? 'Create' : 'Update' }}</button>
    </div>
</form>