@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit product</h3>
            </div>
            <!-- /.box-header -->
            @include('admin.products.form')
        </div>
    </div>
</div>
@endsection