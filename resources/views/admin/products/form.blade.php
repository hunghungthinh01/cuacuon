@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@php($url = is_null($product->id) ? route('products.store') : route('products.update', $product->id))
<form method="post" action="{{ $url }}" class="form-horizontal" enctype="multipart/form-data">
    <div class="box-body no-padding">
        @csrf
        @if(!is_null($product->id))
            @method('PATCH')
        @endif
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="product[name]" value="{{ $product->name }}"/>
            </div>
        </div>

        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Short Des</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="product[short_des]" value="{{ $product->short_des }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Price</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="product[price]" value="{{ $product->price }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Category</label>
            <div class="col-sm-10">
                {!! selectGroupCategory('product[category_id]', $product->category_id) !!}
            </div>
        </div>

        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">URL</label>
            <div class="col-sm-10">
                <p>Ảnh nên được format theo định dạng 500x470 va đuôi jpg để hiển thị đẹp hơn.</p>
                @if (!empty($product->url))
                <img src="{{ asset($product->img('origin')) }}"/>
                @endif
                <input type="file" class="form-control" name="product[url]" accept="image/*"/>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <label class="col-sm-2 control-label">Seo Title</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="product[seo_title]" maxlength="60">{{ $product->seo_title }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Seo Description</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="product[seo_description]" maxlength="158">{{ $product->seo_description }}</textarea>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ route('products.index') }}" class="btn btn-primary pull-left">Cancel</a>
        <button type="submit" class="btn btn-primary pull-right">{{ is_null($product->id) ? 'Create' : 'Update' }}</button>
    </div>
</form>