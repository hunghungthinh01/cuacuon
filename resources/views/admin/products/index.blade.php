@extends('layouts.admin')

@section('content')
<div class="row">
  <div class="col-xs-12">
<form class="form-inline" action="{{route('products.index')}}">
  @csrf
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="name" value="{{Request::query('name')}}">
  </div>
  <div class="form-group">
    <label>Category:</label>
    {!!selectGroupCategory('category_id', Request::query('category_id'), true)!!}
  </div>
  <button type="submit" class="btn btn-default">Tim Kiem</button>
</form>
</div>
</div>

<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Danh sach san pham</h3>
            <div class="box-tools">
               <div class="input-group input-group-sm">
                  <a class="label label-success" href="{{route('products.create')}}">New</a>
               </div>
            </div>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            @if (empty($products))
                Khong co tai san pham nao!
            @else
                <table class="table table-hover">
                   <tbody>
                      <tr>
                         <th>ID</th>
                         <th>Name</th>
                         <th>Avatar</th>
                         <th>Price</th>
                         <th>Category</th>
                         <th>Short Des</th>
                         <th>Type</th>
                         <th></th>
                         <th></th>
                      </tr>
                      @foreach($products as $product)
                      <tr>
                         <td>{{ $product->id }}</td>
                         <td>{{ $product->name }}</td>
                         <td>
                           <img src="{{ asset($product->img()) }}" height="50px" width="50px"/>
                         </td>
                         <td>{{ $product->price }}</td>
                         <td>{{ $product->category->name }}</td>
                         <td>{{ $product->short_des }}</td>
                         <td>{{ $product->type }}</td>
                         <td>
                             <a class="label label-primary" href="{{ route('products.edit', $product->id) }}">Edit</a>
                         </td>
                         <td>
                             <form action="{{ route('products.destroy', $product->id) }}" method="post" class="confirmDeleteRecord">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="label label-danger">Delete</button>
                             </form>
                         </td>
                      </tr>
                      @endforeach
                   </tbody>
                </table>
            @endif
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <div class="center">{{ $products->links() }}</div>
   </div>
</div>
@endsection
