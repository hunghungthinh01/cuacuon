@extends('layouts.admin')

@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Danh sach tin</h3>
            <div class="box-tools">
               <div class="input-group input-group-sm">
                  <a class="label label-success" href="{{route('posts.create')}}">New</a>
               </div>
            </div>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            @if (empty($posts))
                Khong co tin nao!
            @else
                <table class="table table-hover">
                   <tbody>
                      <tr>
                         <th>ID</th>
                         <th>post Name</th>
                         <th>Avatar</th>
                         <th>Redirect to</th>
                         <th>Order</th>
                         <th></th>
                         <th></th>
                      </tr>
                      @foreach($posts as $post)
                      <tr>
                         <td>{{ $post->id }}</td>
                         <td>{{ $post->title }}</td>
                         <td>
                             <a class="label label-primary" href="{{ route('posts.edit', $post->id) }}">Edit</a>
                         </td>
                         <td>
                             <form action="{{ route('posts.destroy', $post->id) }}" method="post" class="confirmDeleteRecord">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="label label-danger">Delete</button>
                             </form>
                         </td>
                      </tr>
                      @endforeach
                   </tbody>
                </table>
            @endif
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <div class="center">{{ $posts->links() }}</div>
   </div>
</div>
@endsection
