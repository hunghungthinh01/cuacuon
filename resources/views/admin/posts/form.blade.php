@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@php($url = is_null($post->id) ? route('posts.store') : route('posts.update', $post->id))
<form method="post" action="{{ $url }}" class="form-horizontal" enctype="multipart/form-data">
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body no-padding">
                @csrf
                @if(!is_null($post->id))
                    @method('PATCH')
                @endif
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="post[title]" value="{{ $post->title }}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="slug" class="col-sm-2 control-label">Slug</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="post[slug]" value="{{ $post->slug }}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Category</label>
                    <div class="col-sm-10">
                        {!! selectPostCategory('post[post_category_id]', $post->post_category_id) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="des" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea type="text" class="form-control" name="post[description]" id="postCkID"/>
                            {{ $post->description }}
                        </textarea>
                    </div>
                </div>

<!--                 <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Avatar</label>
                    <div class="col-sm-10">
                        @if (!empty($post->avatar))
                        <img src="{{ asset($post->avatar) }}" height="100px" width="100px"/>
                        @endif
                        <input type="file" class="form-control" name="post[avatar]" accept="image/*"/>
                    </div>
                </div> -->

                <div class="form-group">
                    <label for="seo_title" class="col-sm-2 control-label">Seo Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="post[seo_title]" value="{{ $post->seo_title }}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Seo Description</label>
                    <div class="col-sm-10">
                        <textarea type="text" class="form-control" name="post[seo_description]"/>
                            {{ $post->seo_description }}
                        </textarea>
                    </div>
                </div>

                <div class="box-footer">
                    <a href="{{ route('posts.index') }}" class="btn btn-primary pull-left">Cancel</a>
                    <button type="submit" class="btn btn-primary pull-right">{{ is_null($post->id) ? 'Create' : 'Update' }}</button>
                </div>
            </div>
        </div>
    </div>
</form>