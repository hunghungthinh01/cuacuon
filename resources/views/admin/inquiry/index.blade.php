@extends('layouts.admin')

@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Danh sach inquiries</h3>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            @if (empty($inquiries))
                Khong co inquiry nao!
            @else
                <table class="table table-hover">
                   <tbody>
                      <tr>
                         <th>Name</th>
                         <th>Email</th>
                         <th>Tel</th>
                         <th>Fax</th>
                         <th>Address</th>
                         <th>Is Sent Mail</th>
                         <th>Content</th>
                      </tr>
                      @foreach($inquiries as $inquiry)
                      <tr>
                         <td>{{ $inquiry->name }}</td>
                         <td>{{ $inquiry->email }}</td>
                         <td>{{ $inquiry->tel }}</td>
                         <td>{{ $inquiry->fax }}</td>
                         <td>{{ $inquiry->address }}</td>
                         <td>{{ $inquiry->is_sent }}</td>
                         <td>{{ $inquiry->content }}</td>
                      </tr>
                      @endforeach
                   </tbody>
                </table>
            @endif
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <div class="center">{{ $inquiries->links() }}</div>
   </div>
</div>
@endsection