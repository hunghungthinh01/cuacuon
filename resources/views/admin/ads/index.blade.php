@extends('layouts.admin')

@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Danh sach quang cao</h3>
            <div class="box-tools">
               <div class="input-group input-group-sm">
                  <a class="label label-success" href="{{route('ads.create')}}">New</a>
                  <a class="label label-success" href="{{ route('ads.clear_cache') }}">Clear Cache</a>
               </div>
            </div>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
            @if (empty($ads))
                Khong co quang cao nao!
            @else
                <table class="table table-hover">
                   <tbody>
                      <tr>
                         <th>ID</th>
                         <th>Title</th>
                         <th>Avatar</th>
                         <th>Redirect to</th>
                         <th>Order</th>
                         <th></th>
                         <th></th>
                      </tr>
                      @foreach($ads as $ad)
                      <tr>
                         <td>{{ $ad->id }}</td>
                         <td>{{ $ad->title }}</td>
                         <td>
                          <img src="{{ asset($ad->avatar) }}" height="100px" width="100px"/>
                         </td>
                         <td>{{ $ad->redirect_to }}</td>
                         <td>{{ $ad->order }}</td>
                         <td>
                             <a class="label label-primary" href="{{ route('ads.edit', $ad->id) }}">Edit</a>
                         </td>
                         <td>
                             <form action="{{ route('ads.destroy', $ad->id) }}" method="post" class="confirmDeleteRecord">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="label label-danger">Delete</button>
                             </form>
                         </td>
                      </tr>
                      @endforeach
                   </tbody>
                </table>
            @endif
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
</div>
@endsection
