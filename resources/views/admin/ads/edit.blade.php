@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Chinh sua quang cao</h3>
            </div>
            <!-- /.box-header -->
            @include('admin.ads.form')
        </div>
    </div>
</div>
@endsection