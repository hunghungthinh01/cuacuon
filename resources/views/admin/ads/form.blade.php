@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@php($url = is_null($ad->id) ? route('ads.store') : route('ads.update', $ad->id))
<form method="post" action="{{ $url }}" class="form-horizontal" enctype="multipart/form-data">
    <div class="box-body no-padding">
        @csrf
        @if(!is_null($ad->id))
            @method('PATCH')
        @endif
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">ad Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="ad[title]" value="{{ $ad->title ? :old('ad.title') }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Avatar</label>
            <div class="col-sm-10">
                <p>Ảnh nên có with=236px, còn heigh thì tùy ý!</p>
                @if (!empty($ad->avatar))
                <img src="{{ asset($ad->avatar) }}"/>
                @endif
                <input type="file" class="form-control" name="ad[avatar]" accept="image/*"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Redirect to</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="ad[redirect_to]" value="{{ $ad->redirect_to ? :old('ad.redirect_to') }}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Order</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="ad[order]" value="{{ $ad->order ? :old('ad.order') }}"/>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ route('ads.index') }}" class="btn btn-primary pull-left">Cancel</a>
        <button type="submit" class="btn btn-primary pull-right">{{ is_null($ad->id) ? 'Create' : 'Update' }}</button>
    </div>
</form>