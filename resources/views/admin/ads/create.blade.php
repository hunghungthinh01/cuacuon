@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Tao quang cao</h3>
            </div>
            <!-- /.box-header -->
            @include('admin.ads.form')
        </div>
    </div>
</div>
@endsection