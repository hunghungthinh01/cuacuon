<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta description="@yield('site_description', Config::get('app.site_description'))">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('site_title', Config::get('app.site_title'))</title>

    <link href="{{ asset('css/common/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('css/common/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="{{ asset('css/common/ionicons.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/frontend/owl.carousel.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/frontend/home.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('css_start')
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
<div class="container">
  <!-- Header Navbar -->
  @php($footer = allSettings())
  <header>
    <div class="header-top">
      <img src="{{ asset($footer['header_top']) }}"/>
    </div>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navmenu_horizontal_id">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                     
          </button>
          <a class="navbar-brand navbar-toggle h2MasterDoor" href="#"><h2>MASTER DOOR</h2></a>
        </div>
        <div class="navbar-collapse collapse" id="navmenu_horizontal_id">
            @include('frontend.navmenu_horizontal')
        </div>
      </div>
    </nav>
  </header>

  <div class="row content">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <div class="navbar-header leftSideBarClass navbar-toggle">
        <button class="navbar-default navbar-toggle" data-toggle="collapse" data-target="#leftSideBar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      @include('frontend.left_sidebar')
    </div>
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <!-- Slide -->
      @include('frontend.top_slide')
      <!-- End slide --> 
      <div class="content">
        @yield('content')
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
      @include('frontend.right_sidebar')
    </div>
  </div>
  @include('frontend.bottom_slide')
  <!-- Main Footer -->
  <footer class="main-footer">
    <!--Footer Links-->
    <div class="row footerTop">
      @include('frontend.footer_top')
    </div>
    <!--/.Footer Links-->
    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">
          <p>{{ $footer['comp_name'] }}</p>
          <p>{{ $footer['comp_addr'] }}</p>
          <p>Tel: <a class="telFormat" href="tel:{{$footer['comp_tel']}}"> {{ $footer['comp_tel'] }}</a> | Fax: {{ $footer['comp_fax'] }} | HOTLINE: <a class="telFormat" href="tel:{{$footer['comp_hotline']}}">{{ $footer['comp_hotline'] }}</a></p>
          <p>Email: <a class="telFormat" href="mailto:{{ $footer['comp_email'] }}">{{ $footer['comp_email'] }}</a> | Web: {{ $footer['comp_website'] }}</p>
          <strong>Copyright &copy; 2018 <a href="#">HungN</a>.</strong> All rights reserved.
        </div>
    </div>
    <!--/.Copyright--> 
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('js/common/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('js/common/bootstrap.min.js') }}"></script>

<script src="{{ asset('js/frontend/owl.carousel.min.js') }}"></script>

<script src="{{ asset('js/frontend/home.js') }}"></script>

@yield('js_end')
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
