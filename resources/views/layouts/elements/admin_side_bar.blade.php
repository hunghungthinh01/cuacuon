<section class="sidebar">
  <!-- Sidebar Menu -->
  <ul class="sidebar-menu" data-widget="tree">
    <!-- Optionally, you can add icons to the links -->
    <li class="active"><a href="{{ route('accounts.index') }}"><i class="fa fa-link"></i>Users Lists</a></li>
    <li>
      <a href="{{ route('partners.index') }}">
        <i class="fa fa-link"></i>
        <span>Đối tác</span>
      </a>
    </li>

    <li>
      <a href="{{ route('ads.index') }}">
        <i class="fa fa-link"></i>
        <span>Quảng cáo</span>
      </a>
    </li>

    <li>
      <a href="{{ route('products.index') }}">
        <i class="fa fa-link"></i>
        <span>Products</span>
      </a>
    </li>

    <li>
      <a href="{{ route('pages.index') }}">
        <i class="fa fa-link"></i>
        <span>Pages</span>
      </a>
    </li>

    <li>
      <a href="{{ route('posts.index') }}">
        <i class="fa fa-link"></i>
        <span>Posts</span>
      </a>
    </li>

    <li>
      <a href="{{ route('slides.index') }}">
        <i class="fa fa-link"></i>
        <span>Slide Lists</span>
      </a>
    </li>

    <li>
      <a href="{{ route('inquiry.index') }}">
        <i class="fa fa-link"></i>
        <span>Danh sách yêu cầu</span>
      </a>
    </li>

    <li>
      <a href="{{ route('categories.index') }}">
        <i class="fa fa-link"></i>
        <span>Danh mục sản phẩm</span>
      </a>
    </li>

    <li>
      <a href="{{ route('setting.index') }}">
        <i class="fa fa-link"></i>
        <span>Setting</span>
      </a>
    </li>

    <li>
      <a href="{{ route('caches.index') }}">
        <i class="fa fa-link"></i>
        <span>Clear Caches</span>
      </a>
    </li>
  </ul>
  <!-- /.sidebar-menu -->
</section>