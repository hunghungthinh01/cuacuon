@extends('layouts.frontend')

@section('site_title', $pcate->name .' | '. Config::get('app.site_title'))

@section('site_description', $pcate->name .' | '. Config::get('app.site_title'))

@section('content')
	@if($pcate)
	<div class="row breadCrumb"> {{ Breadcrumbs::render('post_category', $pcate) }} </div>
	@endif

<div id="products" class="row list-group">
  <div class="panel panel-primary orangeClass">
    <div class="panel-heading">Danh sach tin</div>
    <div class="panel-body">
      @foreach($pcate->posts_with_paginate() as $post)
        <ul>
        	<li><a href="{{ route('post_detail_home', $post->slug) }}">{{ $post->title }}</a></li>
        </ul>
      @endforeach
      <div class="center">{{ $pcate->posts_with_paginate()->links() }}</div>
      @if (!count($pcate->posts))
      	<span>Nội dung đang cập nhật ...</span>
      @endif
    </div>
  </div>
</div>
@endsection
