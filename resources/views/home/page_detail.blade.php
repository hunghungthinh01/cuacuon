@extends('layouts.frontend')

@section('site_title', $page->seo_title)

@section('site_description', $page->seo_description)

@section('content')
	@if($page)
		<div class="row breadCrumb">{{ Breadcrumbs::render('page', $page) }}</div>
	@endif

	<div class="panel panel-primary orangeClass">
	    <div class="panel-heading">{{ $page->title }}</div>
	    <div class="panel-body">
	    	{!! $page->description !!}
	    </div>
	</div>
@endsection
