@extends('layouts.frontend')

@section('site_title', $product->seo_title. ' | ' . Config::get('app.site_title'))

@section('site_description', $product->seo_description. ' | ' . Config::get('app.site_title'))

@section('content')
	@if($product)
	<div class="row breadCrumb">{{ Breadcrumbs::render('product', $product) }}</div>
	@endif

	<div class="panel panel-primary orangeClass">
	    <div class="panel-heading">{{ $product->name }}</div>
	    <div class="panel-body productDetail">
	    	<img src="{{ asset($product->img('origin')) }}"/>
	    	<div> Giá bán: <span class="sellPrice">{{ formatPrice($product->price) }}</span></div>
	    </div>
	</div>

	<div class="panel panel-primary orangeClass">
	    <div class="panel-heading">Sản phẩm cùng loại</div>
	    <div class="panel-body">
	      @foreach($sameProducts as $sameProduct)
	        <div class="item col-xs-6 col-sm-6 col-md-4 col-lg-4">
	          <div class="thumbnail">
	            <div class="imgFrame">
	              <a href="{{ route('product_home', $sameProduct->slug) }}">
	                <img class="group list-group-image" src="{{ asset($sameProduct->img('thumbnail')) }}" alt=""/>
	              </a>
	            </div>
	            <div class="caption">
	              <h4 class="group inner list-group-item-heading">
	                <a href="{{ route('product_home', $sameProduct->slug) }}" data-toggle="tooltip" title="{{$product->name}}">{{ str_limit($product->name, 60) }}</a>
	                <div class="row">
	                  <div class="col-xs-12 col-md-12"><p class="lead">Gia: {{ formatPrice($sameProduct->price) }}</p></div>
	                </div>
	              </h4>
	            </div>
	          </div>
	        </div>
	      @endforeach
	    </div>
	</div>
@endsection
