@extends('layouts.frontend')

@section('site_title', $post->seo_title. ' | ' . Config::get('app.site_title'))

@section('site_description', $post->seo_description. ' | ' . Config::get('app.site_title'))

@section('content')
	@if($post)
	<div class="row breadCrumb">{{ Breadcrumbs::render('post', $post) }}</div>
	@endif

	<div class="panel panel-primary orangeClass">
	    <div class="panel-heading">{{ $post->post_category->name }}</div>
	    <div class="panel-body">
	    	<h3>{{ $post->title }}</h3>
	    	<p>{{ $post->description }}</p>
	    </div>
	</div>
	<h4> Bài mới hơn</h4>
		@foreach(newestPostSameCate($post->post_category->id) as $post_same_cate)
			<li class="newsList">
		        <a href="{{ route('post_detail_home', $post_same_cate->slug) }}">
		          <img src="{{ asset('/image/home/no_image.jpg') }}"/>
		          {{ $post_same_cate->title }}<span class="updatedDate">({{ $post_same_cate->updated_at->format('Y-m-d') }})</span>
		        </a>
		     </li>
		@endforeach
	<hr/>
@endsection
