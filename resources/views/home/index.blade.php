@extends('layouts.frontend')

@section('site_title', null)

@section('site_description', null)

@section('content')
<div class="row list-group productsClass">
  <div class="panel panel-primary orangeClass">
    <div class="panel-heading">Sản phẩm mới</div>
    <div class="panel-body">
      @foreach($products as $product)
        <div class="item col-xs-6 col-sm-6 col-md-4 col-lg-4">
          <div class="thumbnail">
            <div class="imgFrame">
              <a href="{{ route('product_home', $product->slug) }}">
                <img class="group list-group-image" src="{{ asset($product->img('thumbnail')) }}" alt=""/>
              </a>
            </div>
            <div class="caption">
              <h4 class="group inner list-group-item-heading">
                <a href="{{ route('product_home', $product->slug) }}" data-toggle="tooltip" title="{{$product->name}}">{{ str_limit($product->name, 60) }}</a>
                <div class="row">
                  <div class="col-xs-12 col-md-12"><p class="lead">Gia: {{ formatPrice($product->price) }}</p></div>
                </div>
              </h4>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>


<div class="row list-group productsClass">
  <div class="panel panel-primary orangeClass">
    <div class="panel-heading">Tin tức</div>
    <div class="panel-body">

      @foreach(newsList(10) as $new)
      <li class="homeNewItem">
        <a href="{{ route('post_detail_home', $new->slug) }}">
          <img src="{{ asset('/image/home/no_image.jpg') }}"/>
          {{ $new->title }}<span class="updatedDate">({{ $new->updated_at->format('Y-m-d') }})</span>
        </a>
      </li>
      @endforeach
    </div>
  </div>
</div>
@endsection