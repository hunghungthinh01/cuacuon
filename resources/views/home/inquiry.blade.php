@extends('layouts.frontend')

@section('site_title', 'Liên hệ | '. Config::get('app.site_title'))

@section('site_description', 'Liên hệ | '. Config::get('app.site_title'))

@section('content')
<div class="row breadCrumb">{{ Breadcrumbs::render('contact_us') }}</div>
<div class="row">
	<div class="flash-message">
        @foreach (['success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))
          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
    </div>
	<div class="box box-info">
	        <div class="box-header with-border">
	          <h3 class="box-title">Vui lòng nhập thông tin sau</h3>
	        </div>
	        <!-- /.box-header -->
	        <!-- form start -->
	        <form class="form-horizontal" action="{{ route('post_inquiry') }}" method="POST">
	        	@csrf
	          <div class="box-body">
	            <div class="form-group required">
	              <label class="col-sm-2 control-label">Họ tên</label>

	              <div class="col-sm-10">
	                <input type="text" class="form-control" placeholder="name" name="inquiry[name]" value="{{ old('inquiry.name') }}">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Địa chỉ</label>

	              <div class="col-sm-10">
	                <input type="text" class="form-control" name="inquiry[address]" value="{{ old('inquiry.address') }}">
	              </div>
	            </div>

	            <div class="form-group required">
	              <label class="col-sm-2 control-label">Số điện thoại</label>

	              <div class="col-sm-10">
	                <input type="text" class="form-control" name="inquiry[tel]" value="{{ old('inquiry.tel') }}">
	              </div>
	            </div>

	            <div class="form-group">
	              <label class="col-sm-2 control-label">Fax</label>

	              <div class="col-sm-10">
	                <input type="text" class="form-control" name="inquiry[fax]" value="{{ old('inquiry.fax') }}">
	              </div>
	            </div>

	            <div class="form-group required">
	              <label class="col-sm-2 control-label">Email</label>

	              <div class="col-sm-10">
	                <input type="text" class="form-control" name="inquiry[email]" value="{{ old('inquiry.email') }}">
	              </div>
	            </div>

	           <div class="form-group required">
	              <label class="col-sm-2 control-label">Nội dung</label>

	              <div class="col-sm-10">
	                <textarea type="text" class="form-control" name="inquiry[content]">{{ old('inquiry.content') }}</textarea>
	              </div>
	            </div>

	           	<div class="form-group required">
	              <label class="col-sm-2 control-label">Captcha</label>

	              <div class="col-sm-10">
					<input type="text" id="captcha" name="inquiry[captcha]">
					@captcha
	              </div>
	            </div>

	          </div>
	          <!-- /.box-body -->
	          <div class="box-footer">
	            <button type="submit" class="btn btn-info pull-right">Gửi</button>
	          </div>
	          <!-- /.box-footer -->
	        </form>
	      </div>
</div>
@endsection
