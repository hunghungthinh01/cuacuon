@extends('layouts.frontend')

@section('site_title', null)

@section('site_description', null)

@section('content')
  @if(isset($category))
  <div class="row breadCrumb">{{ Breadcrumbs::render('product_category', $category) }}</div>
  @endif
<div class="row list-group productsClass">
  <div class="panel panel-primary orangeClass">
    <div class="panel-heading">{{ $category->name }}</div>
    <div class="panel-body">
      @if (!count($products))
        Không có sản phẩm nào...
      @endif
      @foreach($products as $product)
        <div class="item col-xs-6 col-sm-6 col-md-4 col-lg-4">
          <div class="thumbnail">
            <div class="imgFrame">
              <a href="{{ route('product_home', $product->slug) }}">
                <img class="group list-group-image" src="{{ asset($product->img('thumbnail')) }}" alt=""/>
              </a>
            </div>
            <div class="caption">
              <h4 class="group inner list-group-item-heading">
                <a href="{{ route('product_home', $product->slug) }}" data-toggle="tooltip" title="{{$product->name}}">{{ str_limit($product->name, 60) }}</a>
                <div class="row">
                  <div class="col-xs-12 col-md-12"><p class="lead">Gia: {{ formatPrice($product->price) }}</p></div>
                </div>
              </h4>
            </div>
          </div>
        </div>
      @endforeach
      <div class="center">{{ $products->links() }}</div>
    </div>
  </div>
</div>
@endsection