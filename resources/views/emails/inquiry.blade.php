<p>Đây là email được gửi tự động khi có inquiry tới.</p>
 
<div>
<p><b>Name:</b>&nbsp;{{ $demo->name }}</p>
<p><b>Email:</b>&nbsp;{{ $demo->email }}</p>
<p><b>SDT:</b>&nbsp;{{ $demo->tel }}</p>
<p><b>FAX:</b>&nbsp;{{ $demo->fax }}</p>
<p><b>Address:</b>&nbsp;{{ $demo->address }}</p>
<p><b>Content:</b>&nbsp;{{ $demo->content }}</p>
</div>
 
Thank You!