<!--First column-->
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <h4 class="title mb-4 mt-3 font-bold paddingLeft30px">Giới thiệu về chúng tôi</h4>
    <ul>
        <li><a href="{{ route('page_detail_home', 'luat-cong-ty') }}">Thông điệp từ MasterDoor</a></li>
        <li><a href="{{ route('page_detail_home', 'tam-nhin-xu-menh') }}">Tầm nhìn - Sứ mệnh</a></li>
        <li><a href="{{ route('page_detail_home', 'chinh-sach-chat-luong') }}">Chính sách chất lượng</a></li>
        <li><a href="{{ route('page_detail_home', 'lien-he-voi-chung-toi') }}">Liên hệ với chúng tôi</a></li>
    </ul>
</div>
<!--/.First column-->
<!--Second column-->
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <h4 class="title mb-4 mt-3 font-bold paddingLeft30px">Dịch vụ của chúng tôi</h4>
    <ul>
        <li><a href="{{ route('page_detail_home', 'tu-van-ky-thuat') }}">Tư vấn kỷ thuật</a></li>
        <li><a href="{{ route('page_detail_home', 'thi-cong-lap-dat') }}">Thi công lắp đặt</a></li>
        <li><a href="{{ route('page_detail_home', 'sua-chua-bao-duong') }}">Sửa chữa - bảo dưỡng</a></li>
        <li><a href="{{ route('page_detail_home', 'tu-van-phong-thuy') }}">Tư vấn phong thủy</a></li>
    </ul>
</div>
<!--/.Second column-->
<!--Third column-->
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <h4 class="title mb-4 mt-3 font-bold paddingLeft30px">Cẩm nang</h4>
    <ul>
        <li><a href="{{ route('post_category_home', 'tai-lieu') }}">Tài liệu</a></li>
        <li><a href="{{ route('post_category_home', 'cam-nang-xay-dung') }}">Cẩm nang xây dựng</a></li>
        <li><a href="{{ route('post_category_home', 'cam-nang-nha-dep') }}">Cẩm nang nhà đẹp</a></li>
        <li><a href="{{ route('post_category_home', 'hoi-dap') }}">Hỏi đáp</a></li>
    </ul>
</div>
<!--/.Third column-->