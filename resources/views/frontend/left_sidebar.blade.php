<div class="navbar-collapse collapse" id="leftSideBar">
	<ul class="list-group parentMenu orangeClass sidebar-menu tree list-unstyled">
		@foreach(treeNav() as $cate)
		<li class="treeview list-group-item" data-cosllapse="#cosllapId_{{ $cate['id'] }}">
			<a href="#">
				<span>{{ $cate['name'] }}</span>
				<span class="pull-right-container">
		            <i class="fa fa-angle-left pull-right"></i>
		        </span>
		    </a>
		</li>
		<ul class="treeview-menu childs hide" id="cosllapId_{{ $cate['id'] }}">
			@foreach($cate['childs'] as $child)
			<li class="child list-group-item">
				<i class="fa fa-circle-o"></i>
				<a href="{{ route('category_home', $child['slug']) }}">{{ $child['name'] }}</a>
			</li>
			@endforeach
		</ul>
		@endforeach
	</ul>
</div>
<ul class="list-group partnerMenu orangeClass">
	<li class="list-group-item active">Doi Tac</li>
	@foreach(allPartners() as $partner)
	<li class="center childPartner">
		<a href="{{$partner->redirect_to}}"><img src="{{ asset($partner->avatar) }}" alt=""/></a>
	</li>
	@endforeach
</ul>