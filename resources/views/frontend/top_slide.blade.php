<div id="slideTopId" class="carousel slide row" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    @foreach(allSlides() as $key => $slide)
      <li data-target="#slideTopId" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
    @endforeach
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    @foreach(allSlides() as $key => $slide)
      <div class="item {{ $key == 0 ? ' active' : '' }}">
        <img src="{{ asset($slide->url) }}" alt="{{ $slide->title }}">
      </div>
    @endforeach
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#slideTopId" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#slideTopId" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>