<ul class="nav navbar-nav">
	<li><a class="nav-item" href="{{ route('frontend_root') }}"><span class="glyphicon glyphicon-home"></span></a></li>
	<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			Giới thiệu
			<span class="caret"></span>
		</a>
	    <ul class="dropdown-menu" role="menu">
	      <li class="treeview list-group-item"><a class="" href="{{ route('page_detail_home', 'luat-cong-ty') }}">Giói thiệu</a></li>
	      <li class="treeview list-group-item"><a class="" href="{{ route('page_detail_home', 'co-cau-to-chuc') }}">Cơ cấu tổ chức</a></li>
	      <li class="treeview list-group-item"><a class="" href="{{ route('page_detail_home', 'thong-tin-giao-dich') }}">Thông tin giao dịch</a></li>
	      <li class="treeview list-group-item"><a class="" href="{{ route('page_detail_home', 'doi-tac') }}">Đối tác</a></li>
	    </ul>
	</li>
	<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			Tin Tức
			<span class="caret"></span>
		</a>
	    <ul class="dropdown-menu" role="menu">
	      <li class="treeview list-group-item"><a class="" href="{{ route('post_category_home', 'tin-tu-doanh-nghiep') }}">Tin từ doanh nghiep</a></li>
	      <li class="treeview list-group-item"><a class="" href="{{ route('post_category_home', 'tin-tuc-xay-dung') }}">Tin tức xây dựng</a></li>
	      <li class="treeview list-group-item"><a class="" href="{{ route('post_category_home', 'thong-tin-thi-truong') }}">Thông tin thị trường</a></li>
	    </ul>
	</li>
	<li><a class="blog-nav-item" href="{{ route('category_home') }}">Sản Phẩm</a></li>
	<li class="dropdown"> 
		<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			Tư Vấn
			<span class="caret"></span>
		</a>
	    <ul class="dropdown-menu" role="menu">
	      <li class="treeview list-group-item"><a class="" href="{{ route('post_category_home', 'tu-van-ky-thuat-lap-dat') }}">Tư vấn kỹ thuật lắp đặt</a></li>
	      <li class="treeview list-group-item"><a class="" href="{{ route('post_category_home', 'huong-dan-su-dung') }}">Hướng dẫn sử dụng</a></li>
	      <li class="treeview list-group-item"><a class="" href="{{ route('post_category_home', 'tu-van-phong-thuy') }}">Tư vấn phong thủy</a></li>
	    </ul>
	</li>
	<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			Tuyển Dụng
			<span class="caret"></span>
		</a>
	    <ul class="dropdown-menu" role="menu">
	      <li class="treeview list-group-item">
	      	<a class="" href="{{ route('page_detail_home', 'tuyen-dung') }}">Tuyển dụng 2018</a>
	      </li>
	    </ul>
	</li>
	<li><a class="blog-nav-item" href="{{ route('get_inquiry') }}">Liên Hệ</a></li>
</ul>
