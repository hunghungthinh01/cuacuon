<div class="row">
  <div class="col-md-12">
    <div class="owl-carousel owl-theme" id="theCarousel">
      @foreach(allSlides('bottom') as $key => $slide)
        <div class="item">
            <a href="#">
              <img src="/{{ $slide->url }}" class="{{ $slide->title }}" height="132px">
            </a>
        </div>
      @endforeach
    </div>
  </div>
</div>