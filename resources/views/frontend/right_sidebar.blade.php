<!-- <ul class="list-group orangeClass">
  <li class="list-group-item active">Dịch vụ khách hàng</li>
  @foreach(allServices() as $service)
  	<li class="list-group-item">{{ $service->name }}</li>
  	<li class="list-group-item">{{ $service->phone }}</li>
  @endforeach
</ul> -->

<!-- <ul class="list-group orangeClass">
  <li class="list-group-item active">Bán chạy nhất</li>
  <li class="list-group-item">San</li>
</ul> -->

<ul class="list-group orangeClass">
  <li class="list-group-item active">Tin mới</li>
  @foreach(newsList() as $post)
    <li class="newsList">
      <a href="{{ route('post_detail_home', $post->slug) }}">
        <img src="{{ asset('/image/home/no_image.jpg') }}"/>
        {{ $post->title }}<span class="updatedDate">({{ $post->updated_at->format('Y-m-d') }})</span>
      </a>
    </li>
  @endforeach
</ul>

@if (count(allAds()))
<ul class="list-group orangeClass">
  <li class="list-group-item active">Quảng cáo</li>
  @foreach(allAds() as $ad)
    <li class="center childAd">
      <a href="{{$ad->redirect_to}}"><img src="{{ asset($ad->avatar) }}" alt="{{ asset($ad->title) }}"/></a>
    </li>
  @endforeach
</ul>
@endif